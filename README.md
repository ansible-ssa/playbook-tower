# Ansible Playbook: Ansible Tower

This repository creates an Ansible Tower VM, configures the dependencies and, configures and installs the application. It will also configure some default content (Jobs, Credentials, Projects) which can be used for demonstration purposes.

Read the full documentation on [Ansible Labs](https://www.ansible-labs.de/).

## Environment for Google Compute Platform

If you want to use Google as a provider, you should setup the necessary environment variables. The easiest way is to put them into a file in the sub folder "environments" and source them before running this playbook.

        source environments/GCP

Check the example file in the folder.

## Variables

There is a list of Variables which have to be specified. Check the group_vars/all/main.yml folder for more details. Make sure to create a vault.yml with the respective encrypted variables in the same folder (if you want to use vault, which is highly recommended).

## Execute the playbook

After setting up all the variables, use the following command line:

        ansible-playbook -i hosts tower_install.yml
